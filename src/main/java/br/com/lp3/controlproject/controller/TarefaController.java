package br.com.lp3.controlproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.lp3.controlproject.dto.TarefaDTO;
import br.com.lp3.controlproject.service.TarefaService;



@RestController
@RequestMapping(value = "/tarefa")
public class TarefaController {
	
	@Autowired
	private TarefaService tarefaService;
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public ResponseEntity<TarefaDTO> findTarefaById(@PathVariable Long id) {
		TarefaDTO tarefaDTO = tarefaService.findById(id);
		if (tarefaDTO != null) {
			return new ResponseEntity<TarefaDTO>(tarefaDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<TarefaDTO>(tarefaDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="", method = RequestMethod.POST)
	public ResponseEntity<TarefaDTO> saveTarefa(@RequestBody TarefaDTO tarefaDTO) {
		tarefaDTO = tarefaService.saveTarefa(tarefaDTO);
		if (tarefaDTO != null) {
			return new ResponseEntity<TarefaDTO>(tarefaDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<TarefaDTO>(tarefaDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value="", method = RequestMethod.PUT)
	public ResponseEntity<TarefaDTO> editTarefa(@RequestBody TarefaDTO tarefaDTO) {
		tarefaDTO = tarefaService.saveTarefa(tarefaDTO);
		if (tarefaDTO != null) {
			return new ResponseEntity<TarefaDTO>(tarefaDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<TarefaDTO>(tarefaDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value="", method = RequestMethod.DELETE)
	public ResponseEntity<TarefaDTO> deleteTarefa(@RequestBody TarefaDTO tarefaDTO) {
		tarefaDTO = tarefaService.saveTarefa(tarefaDTO);
		if (tarefaDTO != null) {
			return new ResponseEntity<TarefaDTO>(tarefaDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<TarefaDTO>(tarefaDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	
}