package br.com.lp3.controlproject.dto;

import java.io.Serializable;

import br.com.lp3.controlproject.model.Status;
import br.com.lp3.controlproject.model.Tarefa;
import br.com.lp3.controlproject.model.Usuario;

public class AtividadeDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String descricao;
	private StatusDTO status;
	private UsuarioDTO usuario;
	private TarefaDTO tarefa;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public StatusDTO getStatus() {
		return status;
	}
	public void setStatus(StatusDTO status) {
		this.status = status;
	}
	public UsuarioDTO getUsuario() {
		return usuario;
	}
	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}
	public TarefaDTO getTarefa() {
		return tarefa;
	}
	public void setTarefa(TarefaDTO tarefa) {
		this.tarefa = tarefa;
	}
	public AtividadeDTO(String descricao, StatusDTO status, UsuarioDTO usuario, TarefaDTO tarefa) {
		super();
		this.descricao = descricao;
		this.status = status;
		this.usuario = usuario;
		this.tarefa = tarefa;
	}
	public AtividadeDTO(int id, String descricao, StatusDTO status, UsuarioDTO usuario, TarefaDTO tarefa) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.status = status;
		this.usuario = usuario;
		this.tarefa = tarefa;
	}
	
	
	
	
}
