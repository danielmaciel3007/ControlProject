package br.com.lp3.controlproject.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.lp3.controlproject.model.Tarefa;

public interface TarefaRepository extends CrudRepository<Tarefa, Long>{
	

}