package br.com.lp3.controlproject.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.lp3.controlproject.dto.UsuarioDTO;
import br.com.lp3.controlproject.model.Usuario;
import br.com.lp3.controlproject.repository.UsuarioRepository;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	public UsuarioDTO findById(int id) {
		Optional<Usuario> oUsuario = usuarioRepository.findById(id);
		if (oUsuario != null && oUsuario.isPresent()) {
			Usuario usuario = oUsuario.get();
			UsuarioDTO usuarioDTO = new UsuarioDTO(usuario.getId(), usuario.getNome(), usuario.getEmail(), usuario.getSenha());
			return usuarioDTO;
		}else {
			return null;
		}
	}
	
	public UsuarioDTO saveUsuario(UsuarioDTO usuarioDTO) {
		Usuario usuario = new Usuario(usuarioDTO.getId(), usuarioDTO.getNome(), usuarioDTO.getEmail(), usuarioDTO.getSenha());
		usuario = usuarioRepository.save(usuario);
		usuarioDTO.setId(usuario.getId());
		return usuarioDTO;
	}
}
