package br.com.lp3.controlproject.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.lp3.controlproject.dto.TarefaDTO;
import br.com.lp3.controlproject.helper.ConvertEntity;
import br.com.lp3.controlproject.model.Tarefa;
import br.com.lp3.controlproject.repository.TarefaRepository;


@Service
public class TarefaService {

	@Autowired
	private TarefaRepository tarefaRepository;
	
	public TarefaDTO findById(Long id) {
		Optional<Tarefa> oTarefa = tarefaRepository.findById(id);
		if (oTarefa != null && oTarefa.isPresent()) {
			Tarefa tarefa = oTarefa.get();
			TarefaDTO tarefaDTO = new TarefaDTO(tarefa.getId(),
					tarefa.getTitulo(),
					tarefa.getDescricao(),
					ConvertEntity.ToProjetoDTO(tarefa.getProjeto()),
					tarefa.getDataCriacao(),
					ConvertEntity.ToStatusDTO(tarefa.getStatus()));
			
			return tarefaDTO;
		}else {
			return null;
		}
	}
	
	public TarefaDTO saveTarefa(TarefaDTO tarefaDTO) {
		Tarefa tarefa = new Tarefa(tarefaDTO.getId(),
				tarefaDTO.getTitulo(),
				tarefaDTO.getDescricao(),
				ConvertEntity.ToProjeto(tarefaDTO.getProjeto()),
				tarefaDTO.getDataCriacao(),
				ConvertEntity.ToStatus(tarefaDTO.getStatus()));
		
		tarefa = tarefaRepository.save(tarefa);
		tarefaDTO.setId(tarefa.getId());
		return tarefaDTO;
	}
	
}