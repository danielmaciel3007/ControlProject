package br.com.lp3.controlproject.helper;

import br.com.lp3.controlproject.dto.ProjetoDTO;
import br.com.lp3.controlproject.dto.StatusDTO;
import br.com.lp3.controlproject.dto.TarefaDTO;
import br.com.lp3.controlproject.dto.UsuarioDTO;
import br.com.lp3.controlproject.model.Projeto;
import br.com.lp3.controlproject.model.Status;
import br.com.lp3.controlproject.model.Tarefa;
import br.com.lp3.controlproject.model.Usuario;

public final class ConvertEntity {
	
	 private ConvertEntity () {
		 
	 }

	public static Status ToStatus(StatusDTO dto){
		
		Status status = new Status();
		status.setId(dto.getId());
		status.setStatus(dto.getStatus());
		
		return status;
	}
	
	public static StatusDTO ToStatusDTO(Status status){
		
		StatusDTO dto = new StatusDTO();
		dto.setId(status.getId());
		dto.setStatus(status.getStatus());
		
		return dto;
	}
	
	public static Usuario ToUsuario(UsuarioDTO dto){ 
		
		Usuario usuario = new Usuario();
		usuario.setId(dto.getId());
		usuario.setNome(dto.getNome());
		usuario.setEmail(dto.getEmail());
		usuario.setSenha(dto.getSenha());
		
		return usuario;
	}
	
	public static UsuarioDTO ToUsuarioDTO(Usuario usuario){ 
		
		UsuarioDTO dto = new UsuarioDTO();
		dto.setId(usuario.getId());
		dto.setNome(usuario.getNome());
		dto.setEmail(usuario.getEmail());
		dto.setSenha(usuario.getSenha());
		
		return dto;
	}
	
	public static Projeto ToProjeto(ProjetoDTO dto){
		
		Projeto projeto = new Projeto();
		projeto.setId(dto.getId());
		projeto.setNome(dto.getNome());
		projeto.setDescricao(dto.getDescricao());
		projeto.setStatus(ToStatus(dto.getStatus()));
		projeto.setDataInicio(dto.getDataInicio());
		projeto.setDataLimite(dto.getDataLimite());
		
		return projeto;
	}
	
	public static ProjetoDTO ToProjetoDTO(Projeto projeto){
		
		ProjetoDTO dto = new ProjetoDTO();
		dto.setId(projeto.getId());
		dto.setNome(projeto.getNome());
		dto.setDescricao(projeto.getDescricao());
		dto.setStatus(ToStatusDTO(projeto.getStatus()));
		dto.setDataInicio(projeto.getDataInicio());
		dto.setDataLimite(projeto.getDataLimite());
		
		return dto;
	}

	public static Tarefa ToTarefa(TarefaDTO dto){
		
		Tarefa tarefa = new Tarefa();
		tarefa.setId(dto.getId());
		tarefa.setTitulo(dto.getTitulo());
		tarefa.setDescricao(dto.getDescricao());
		tarefa.setDataCriacao(dto.getDataCriacao());
		tarefa.setProjeto(ToProjeto(dto.getProjeto()));
		tarefa.setStatus(ToStatus(dto.getStatus()));
		
		return tarefa;
	}
	
	public static TarefaDTO ToTarefaDTO(Tarefa tarefa){
		
		TarefaDTO dto = new TarefaDTO();
		dto.setId(tarefa.getId());
		dto.setTitulo(tarefa.getTitulo());
		dto.setDescricao(tarefa.getDescricao());
		dto.setDataCriacao(tarefa.getDataCriacao());
		dto.setProjeto(ToProjetoDTO(tarefa.getProjeto()));
		dto.setStatus(ToStatusDTO(tarefa.getStatus()));
		
		return dto;
	}
}


