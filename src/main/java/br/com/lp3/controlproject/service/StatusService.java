package br.com.lp3.controlproject.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.lp3.controlproject.dto.StatusDTO;
import br.com.lp3.controlproject.model.Status;
import br.com.lp3.controlproject.repository.StatusRepository;



@Service
public class StatusService {

	@Autowired
	private StatusRepository statusRepository;
	
	public StatusDTO findById(Long id) {
		Optional<Status> oStatus = statusRepository.findById(id);
		if (oStatus != null && oStatus.isPresent()) {
			Status status = oStatus.get();
			StatusDTO statusDTO = new StatusDTO(status.getId(), status.getStatus());
			return statusDTO;
		}else {
			return null;
		}
	}
	
	public StatusDTO saveStatus(StatusDTO statusDTO) {
		Status status = new Status(statusDTO.getId(), statusDTO.getStatus());
		status = statusRepository.save(status);
		statusDTO.setId(status.getId());
		return statusDTO;
	}
	
}