package br.com.lp3.controlproject.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.lp3.controlproject.model.Status;

@Repository
public interface StatusRepository extends CrudRepository<Status, Long> {


}