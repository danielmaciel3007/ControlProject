package br.com.lp3.controlproject.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.lp3.controlproject.dto.ProjetoDTO;
import br.com.lp3.controlproject.helper.ConvertEntity;
import br.com.lp3.controlproject.model.Projeto;
import br.com.lp3.controlproject.repository.ProjetoRepository;



@Service
public class ProjetoService {

	@Autowired
	private ProjetoRepository projetoRepository;
	
	public ProjetoDTO findById(Long id) {
		Optional<Projeto> oProjeto = projetoRepository.findById(id);
		if (oProjeto != null && oProjeto.isPresent()) {
			Projeto projeto = oProjeto.get();
			ProjetoDTO projetoDTO = new ProjetoDTO(projeto.getId(),
					projeto.getNome(),
					projeto.getDescricao(),
					projeto.getDataInicio(),
					projeto.getDataLimite(),
				    ConvertEntity.ToStatusDTO(projeto.getStatus()));
			
			return projetoDTO;
		}else {
			return null;
		}
	}
	
	public ProjetoDTO saveProjeto(ProjetoDTO projetoDTO) {
		Projeto projeto = new Projeto(projetoDTO.getId(),
				projetoDTO.getNome(),
				projetoDTO.getDescricao(),
				projetoDTO.getDataInicio(),
				projetoDTO.getDataLimite(),
				ConvertEntity.ToStatus(projetoDTO.getStatus()));
		
		projeto = projetoRepository.save(projeto);
		projetoDTO.setId(projeto.getId());
		return projetoDTO;
	}
	
	
}