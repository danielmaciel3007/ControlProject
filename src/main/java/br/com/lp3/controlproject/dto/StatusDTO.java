package br.com.lp3.controlproject.dto;

import java.io.Serializable;

public class StatusDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String Status;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public StatusDTO(int id, String status) {
		super();
		this.id = id;
		Status = status;
	}
	public StatusDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
}
