package br.com.lp3.controlproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.lp3.controlproject.dto.UsuarioDTO;
import br.com.lp3.controlproject.service.UsuarioService;

@RestController
@RequestMapping(value = "/usuario")
public class UsuarioController {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public ResponseEntity<UsuarioDTO> findUsuarioById(@PathVariable int id) {
		UsuarioDTO usuarioDTO = usuarioService.findById(id);
		if (usuarioDTO != null) {
			return new ResponseEntity<UsuarioDTO>(usuarioDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<UsuarioDTO>(usuarioDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="", method = RequestMethod.POST)
	public ResponseEntity<UsuarioDTO> saveUsuario(@RequestBody UsuarioDTO usuarioDTO) {
		usuarioDTO = usuarioService.saveUsuario(usuarioDTO);
		if (usuarioDTO != null) {
			return new ResponseEntity<UsuarioDTO>(usuarioDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<UsuarioDTO>(usuarioDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value="", method = RequestMethod.PUT)
	public ResponseEntity<UsuarioDTO> editUsuario(@RequestBody UsuarioDTO usuarioDTO) {
		usuarioDTO = usuarioService.saveUsuario(usuarioDTO);
		if (usuarioDTO != null) {
			return new ResponseEntity<UsuarioDTO>(usuarioDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<UsuarioDTO>(usuarioDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value="", method = RequestMethod.DELETE)
	public ResponseEntity<UsuarioDTO> deleteUsuario(@RequestBody UsuarioDTO usuarioDTO) {
		usuarioDTO = usuarioService.saveUsuario(usuarioDTO);
		if (usuarioDTO != null) {
			return new ResponseEntity<UsuarioDTO>(usuarioDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<UsuarioDTO>(usuarioDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}