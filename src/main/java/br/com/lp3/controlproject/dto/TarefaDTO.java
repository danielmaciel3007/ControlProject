package br.com.lp3.controlproject.dto;

import java.io.Serializable;
import java.util.Date;

import br.com.lp3.controlproject.model.Projeto;
import br.com.lp3.controlproject.model.Status;

public class TarefaDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private int id;
	private String Titulo;
	private String Descricao;
	private ProjetoDTO projeto;
	private Date dataCriacao;
	private StatusDTO status;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitulo() {
		return Titulo;
	}
	public void setTitulo(String titulo) {
		Titulo = titulo;
	}
	public String getDescricao() {
		return Descricao;
	}
	public void setDescricao(String descricao) {
		Descricao = descricao;
	}
	public ProjetoDTO getProjeto() {
		return projeto;
	}
	public void setProjeto(ProjetoDTO projeto) {
		this.projeto = projeto;
	}
	public Date getDataCriacao() {
		return dataCriacao;
	}
	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}
	public StatusDTO getStatus() {
		return status;
	}
	public void setStatus(StatusDTO status) {
		this.status = status;
	}
	public TarefaDTO(int id, String titulo, String descricao, ProjetoDTO projeto, Date dataCriacao, StatusDTO status) {
		super();
		this.id = id;
		Titulo = titulo;
		Descricao = descricao;
		this.projeto = projeto;
		this.dataCriacao = dataCriacao;
		this.status = status;
	}
	public TarefaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
}