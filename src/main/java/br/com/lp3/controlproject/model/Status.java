package br.com.lp3.controlproject.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Status {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String Status;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public Status(int id, String status) {
		super();
		this.id = id;
		Status = status;
	}
	public Status() {
		super();
		// TODO Auto-generated constructor stub
	}
		
}
