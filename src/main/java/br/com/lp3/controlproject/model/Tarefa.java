package br.com.lp3.controlproject.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Tarefa {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String Titulo;
	private String Descricao;
	
	@ManyToOne @JoinColumn(name="projeto")
	private Projeto projeto;
	private Date dataCriacao;
	
	@ManyToOne @JoinColumn(name="status")
	private Status status;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitulo() {
		return Titulo;
	}
	public void setTitulo(String titulo) {
		Titulo = titulo;
	}
	public String getDescricao() {
		return Descricao;
	}
	public void setDescricao(String descricao) {
		Descricao = descricao;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public Date getDataCriacao() {
		return dataCriacao;
	}
	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public Tarefa(int id, String titulo, String descricao, Projeto projeto, Date dataCriacao, Status status) {
		super();
		this.id = id;
		Titulo = titulo;
		Descricao = descricao;
		this.projeto = projeto;
		this.dataCriacao = dataCriacao;
		this.status = status;
	}
	public Tarefa() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}