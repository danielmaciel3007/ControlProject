package br.com.lp3.controlproject.dto;

import java.io.Serializable;
import java.util.Date;

import br.com.lp3.controlproject.model.Status;

public class ProjetoDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private int id;
	private String Nome;
	private String Descricao;
	private Date dataInicio;
	private Date dataLimite;
	private StatusDTO status;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return Nome;
	}
	public void setNome(String nome) {
		Nome = nome;
	}
	public String getDescricao() {
		return Descricao;
	}
	public void setDescricao(String descricao) {
		Descricao = descricao;
	}
	public Date getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public Date getDataLimite() {
		return dataLimite;
	}
	public void setDataLimite(Date dataLimite) {
		this.dataLimite = dataLimite;
	}
	public StatusDTO getStatus() {
		return status;
	}
	public void setStatus(StatusDTO status) {
		this.status = status;
	}
	public ProjetoDTO(int id, String nome, String descricao, Date dataInicio, Date dataLimite, StatusDTO status) {
		super();
		this.id = id;
		Nome = nome;
		Descricao = descricao;
		this.dataInicio = dataInicio;
		this.dataLimite = dataLimite;
		this.status = status;
	}
	public ProjetoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
}