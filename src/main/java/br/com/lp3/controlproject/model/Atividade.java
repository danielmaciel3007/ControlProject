package br.com.lp3.controlproject.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Atividade {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String descricao;
	@ManyToOne @JoinColumn(name="status")
	private Status status;
	@ManyToOne @JoinColumn(name="usuario")
	private Usuario usuario;
	@ManyToOne @JoinColumn(name="tarefa")
	private Tarefa tarefa;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Tarefa getTarefa() {
		return tarefa;
	}
	public void setTarefa(Tarefa tarefa) {
		this.tarefa = tarefa;
	}
	public Atividade(String descricao, Status status, Usuario usuario, Tarefa tarefa) {
		super();
		this.descricao = descricao;
		this.status = status;
		this.usuario = usuario;
		this.tarefa = tarefa;
	}
	public Atividade(int id, String descricao, Status status, Usuario usuario, Tarefa tarefa) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.status = status;
		this.usuario = usuario;
		this.tarefa = tarefa;
	}
	
	
}
