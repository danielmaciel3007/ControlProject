package br.com.lp3.controlproject.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.lp3.controlproject.model.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {

}
