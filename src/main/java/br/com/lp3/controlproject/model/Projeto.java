package br.com.lp3.controlproject.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Projeto {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String Nome;
	private String Descricao;
	private Date dataInicio;
	private Date dataLimite;
	@ManyToOne @JoinColumn(name="status")
	private Status status;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return Nome;
	}
	public void setNome(String nome) {
		Nome = nome;
	}
	public String getDescricao() {
		return Descricao;
	}
	public void setDescricao(String descricao) {
		Descricao = descricao;
	}
	public Date getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public Date getDataLimite() {
		return dataLimite;
	}
	public void setDataLimite(Date dataLimite) {
		this.dataLimite = dataLimite;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public Projeto(int id, String nome, String descricao, Date dataInicio, Date dataLimite, Status status) {
		super();
		this.id = id;
		Nome = nome;
		Descricao = descricao;
		this.dataInicio = dataInicio;
		this.dataLimite = dataLimite;
		this.status = status;
	}
	public Projeto() {
		super();
		// TODO Auto-generated constructor stub
	}
		
}